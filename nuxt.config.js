const pkg = require("./package")

module.exports = {
  mode: "universal",

  env: {
    apiBaseUrl: process.env.API_BASE_URL || "http://localhost:3000/",
    socketUrl: process.env.SOCKET_URL || "http://localhost:4000"
  },

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: pkg.description }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  auth: {
    redirect: {
      callback: "/callback"
    },
    strategies: {
      google: {
        client_id:
          "640415042752-59tf6h4lqu2nqkbavmc6qfmeuvhargus.apps.googleusercontent.com"
      }
    }
  },

  router: {
    middleware: ["auth"]
  },

  /*
   ** Global CSS
   */
  css: ["~/assets/style/app.styl"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "@/plugins/vuebar.js",
    { src: "@/plugins/vue-socket-io.js", ssr: false },
    "@/plugins/vuetify",
    "@/plugins/vue-youtube.js"
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "@/modules/typescript.js"
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        })
      }
    }
  }
}
