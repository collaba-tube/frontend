import axios, { AxiosInstance } from 'axios'

class Api {
  readonly url: string
  private axios: AxiosInstance
  constructor (url: string) {
    this.url = url
    this.axios = axios.create({
      baseURL: url
    })
  }

  public getUsers () {
    return this.axios.get('users')
  }

  public getMe () {
    return this.axios.get('me', {withCredentials: true})
  }

  public getRooms () {
    return this.axios.get('rooms')
  }

  public addRoom (name: string) {
    // return this.axios.post('rooms', {name, creator: '/users/1'})
    return this.axios.post('rooms', {name})
  }

  public getVideos (id: number) {
    // return this.axios.get(`videos?room=${id}`)
    return this.axios.get(`rooms/${id}/videos`)
  }

  public addVideo (vid, room: number) {
    // return this.axios.post('videos', {vid, room: `/rooms/${room}`, creator: '/users/1'})
    return this.axios.post('videos', {vid, room: `/rooms/${room}`})
  }

  public addVideoByUrl (url, roomId: number) {
    return this.axios.post('videos', {url, roomId})
  }
}

export default new Api(process.env.apiBaseUrl || '')
