FROM keymetrics/pm2:latest-alpine
LABEL MAINTAINER="instane@gmail.com"
LABEL version="1.2.1"

ENV PORT 4000
ENV HOST 0.0.0.0

ENV API_BASE_URL https://api.collabatube.com
ENV SOCKET_URL https://socket.collabatube.com

RUN mkdir -p /app
WORKDIR /app

COPY ./ /app

RUN yarn install --non-interactive --silent && yarn build

CMD [ "pm2-runtime", "start", "ecosystem.config.js", "--env", "production" ]
EXPOSE 4000
