export const state = () => ({
  position: 0,
  duration: 0,
  muted: process.browser && localStorage.muted ? localStorage.muted : false,
  volume: process.browser && localStorage.volume ? localStorage.volume : 10,
  playing: true,
  playlist: [],
  videoNum: 0,
  rooms: [],
  currentRoom: 0,
  errors: [],
  currentQuality: ""
})

export const mutations = {
  setCurrentQuality(state, currentQuality) {
    state.currentQuality = currentQuality
  },
  setCurrentRoom(state, currentRoom) {
    state.currentRoom = currentRoom
  },
  nextVideo(state) {
    if (++state.videoNum > state.playlist.length - 1) {
      state.videoNum = 0
    }
  },
  setRooms(state, rooms) {
    state.rooms = rooms
  },
  addRoom(state, room) {
    state.rooms.push(room)
  },
  setPlaylist(state, playlist) {
    const playingVideo = playlist.find(video => {
      return video.playing
    })
    if (playingVideo) {
      const currentTimestamp = Math.round(new Date().getTime() / 1000)
      const beginAtTimestamp = Math.round(
        new Date(playingVideo.beginAt).getTime() / 1000
      )
      state.position = currentTimestamp - beginAtTimestamp
      // console.log('playingVideo', playingVideo, beginAtTimestamp, currentTimestamp, currentTimestamp - beginAtTimestamp)
    }
    state.playlist = playlist
  },
  addToPlaylist(state, video) {
    state.playlist.push(video)
  },
  setVolume(state, volume) {
    state.volume = volume
    localStorage.volume = volume
  },
  setPosition(state, position) {
    state.position = position
  },
  incrementPosition(state) {
    state.position += 1
  },
  setDuration(state, duration) {
    state.duration = duration
  },
  mute(state) {
    state.muted = true
    localStorage.muted = true
  },
  unMute(state) {
    state.muted = false
    localStorage.muted = false
  },
  toggleMute(state) {
    state.muted = !state.muted
    localStorage.muted = !localStorage.muted
  },
  play(state) {
    state.playing = true
  },
  pause(state) {
    state.playing = false
  },
  togglePlaying(state) {
    state.playing = !state.playing
  },
  addError(state, error) {
    state.errors.unshift(error)
  },
  popError(state) {
    state.errors.pop()
  }
}

export const getters = {
  playlist(state) {
    return state.playlist
  },
  errors(state) {
    return state.errors
  },
  positionHuman(state) {
    const date = new Date(null)
    date.setSeconds(state.position)
    return date.toISOString().substr(11, 8)
  },
  currentVideo(state) {
    return state.playlist.find(video => {
      return video.playing
    })
    // return state.playlist[state.videoNum]
  },
  room(state) {
    return state.rooms[state.currentRoom]
  }
}

export const actions = {
  populateErrors({ commit }, error) {
    commit("addError", error)
    setTimeout(() => commit("popError"), 3000)
  }
}
