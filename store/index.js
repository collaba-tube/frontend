export const state = () => ({
  sidebar: false,
  users: 0,
  rooms: [],
  messages: [],
  me: {},
  userNames: []
})

export const mutations = {
  setMe(state, me) {
    state.me = me
  },
  toggleSidebar(state) {
    state.sidebar = !state.sidebar
  },
  setRooms(state, rooms) {
    state.rooms = [
      ...rooms.map(room => {
        return { ...room, users: 0 }
      })
    ]
  },
  addRoom(state, room) {
    state.rooms.push({ ...room, users: 0 })
  },
  clearMessages(state) {
    state.messages = []
  },
  SOCKET_userCountChange(state, { users }) {
    state.users = users
  },
  SOCKET_roomUserCountChange(state, { roomId, users, userNames }) {
    const roomIndex = state.rooms.findIndex(_room => +_room.id === +roomId)
    console.log("userNames", userNames)
    state.userNames = userNames

    if (roomIndex === -1) {
      return
    }
    state.rooms[roomIndex].users = users
    state.rooms[roomIndex].userNames = userNames
  },
  SOCKET_roomUsersCount(state, { rooms }) {
    if (rooms === undefined) {
      return
    }
    Object.keys(rooms).map(id => {
      const roomIndex = state.rooms.findIndex(_room => +_room.id === +id)
      if (roomIndex === -1) {
        return
      }
      state.rooms[roomIndex].users = rooms[id]
    })
  },
  SOCKET_newMessage(state, { username, text }) {
    state.messages = [...state.messages, { username, text }]
  }
}
