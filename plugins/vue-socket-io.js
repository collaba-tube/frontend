import Vue from "vue"
import VueSocketIO from "vue-socket.io"

export default ({ app }) => {
  const ws = new VueSocketIO({
    debug: true,
    connection: process.env.socketUrl,
    vuex: {
      store: app.store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_"
    }
  })
  Vue.use(ws)
}
